class Menu:
    menu = {
        '+': 'Сложение',
        '-': 'Вычитание',
        '*': 'Умножение',
        '/': 'Деление',
    }

    @classmethod
    def show(cls):
        '''
        Показать допустимые команды меню
        '''
        for key, value in cls.menu.items():
            print(f"{key} - {value}")
        print("Для выхода нажмите Ctrl + C")

    @classmethod
    def check_command(cls, command):
        '''
        Проверить существование команды
        '''
        return command in cls.menu.keys()
