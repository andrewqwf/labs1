from urllib.request import urlopen


class UrlReader:
    '''Класс для чтения данных по url'''

    @classmethod
    def read(cls, path):
        '''Чтение данных'''
        try:
            file = urlopen(path)
        except Exception:
            raise Exception(f"Произошла ошибка при загрузке файла: {path}")
        else:
            return file.readline()
