import os

import matplotlib.pyplot as plt

CHART_FILE_NAME = 'chart.png'
CHART_PATH = f"{os.getcwd()}/{CHART_FILE_NAME}"


class Chart:
    '''График'''
    @classmethod
    def draw(cls, data, sma_data):
        '''Рисование график'''
        plt.plot(range(len(sma_data)), sma_data, range(len(data)), data)
        plt.ylabel('Y')
        plt.xlabel('X')
        plt.savefig(CHART_PATH)
        print(f"Для просмотра графика откройте файл: {CHART_PATH}")
