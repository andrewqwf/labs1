class SMA:

    def __call__(self, data, frame):
        '''Вычисление sma'''
        result = []
        for i in range(1, len(data) + 1):
            if i >= frame:
                result.append(sum(data[i - frame: frame + (i - frame)]) / frame)
        return result
