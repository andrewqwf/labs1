from .polinom import Polinom


SCHEMA = '1/{}*3'


class PolinomManager:
    '''
    Менеджер по управлению полиномом
    '''

    def __init__(self, values):
        self.values = []
        for value in values:
            if value.isdigit():
                self.values.append(float(value))
            else:
                raise ValueError(f"Неверное значение: '{value}', значение должно быть числом")

    def calc_result(self):
        '''
        Подсчитать результат
        '''
        result = 0
        polinom = Polinom()
        for value in self.values:
            try:
                result += polinom(SCHEMA, value)
            except ZeroDivisionError:
                raise
        return result
