import argparse

from polinom import PolinomManager

parser = argparse.ArgumentParser(description='polinom_lab_3')
parser.add_argument('--poly=', action="store", required=True,
                    dest="args", help="Значения полинома через запятую")
args = parser.parse_args().args.split(',')

try:
    polinom = PolinomManager(args)
except ValueError as e:
    print(e)
    exit()

try:
    result = polinom.calc_result()
except ZeroDivisionError:
    print('Попытка деления на 0')
else:
    print(f'Результат выполнения полинома: {result}')
