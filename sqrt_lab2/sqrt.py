BEGIN_APPRIXIMATION = 1.0
ACCURACY = 10**-6


class SQRT:

    def __call__(self, value):
        if not value.isdigit():
            raise ValueError('Значение должно быть числом')

        value = float(value)
        if value < 0:
            raise ValueError('Значение должно быть больше 0')

        # стартовое приближение
        approximation = BEGIN_APPRIXIMATION

        # поиск корня по формуле Герона
        while not SQRT.check(value, approximation):
            approximation = SQRT.iteration(value, approximation)

        return approximation if approximation % 1 > 0.001 else approximation // 1

    @staticmethod
    def iteration(value, approximation):
        '''Итерация вычисления квадратного корня'''
        return 0.5 * (approximation + value / approximation)

    @staticmethod
    def check(value, approximation):
        '''Проверка точности числа методом Ньютона'''
        return abs(value - approximation**2) < ACCURACY
