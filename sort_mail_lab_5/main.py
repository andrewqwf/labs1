import argparse

from chart import Chart
from reader import Reader
from stats import Statistics

parser = argparse.ArgumentParser(description='sort_mail_lab_5')
parser.add_argument('--path=', action="store", required=True,
                    dest="path", help="путь до файла")
path = parser.parse_args().path

# чтение данных
reader = Reader()
try:
    data = reader(path)
except FileNotFoundError as e:
    print(e)
    exit()
except Exception:
    print('Произошла ошибка чтения файла')
    exit()

# анализ данных
statistics = Statistics()
try:
    data = statistics.analyze_data(data)
except Exception:
    print('Произошла ошибка при сборе информации')
    exit()
else:
    # распечатываем список спамеров
    statistics.print_spammers()
    # рисуем гистограмму
    Chart.draw(data)
