import os
import re

AUTHOR_REGEXP = r'Author: .*'
X_DSPAM_CONFIDENCE_REGEXP = r'X-DSPAM-Confidence: .*'
X_DSPAM_PROBABILITY_REGEXP = r'X-DSPAM-Probability: .*'

DELIMETR = '----------------------'


class Reader:
    '''Класс для чтения файла'''

    def __call__(self, path):
        if not os.path.exists(path):
            raise FileNotFoundError('Файл не найден')

        # чтение файла
        blocks = []
        with open(path, 'r') as f:
            block = {}
            # читаем файл по строчно
            for line in f:
                # достигли разделителя
                if DELIMETR in line and block:
                    blocks.append(block)
                    block = {}
                    continue
                # нашли Author
                if re.match(AUTHOR_REGEXP, line):
                    value = re.search(AUTHOR_REGEXP, line).group(0).split(' ')[1]
                    block['author'] = value
                    continue
                # нашли X-DSPAM-Confidence
                if re.match(X_DSPAM_CONFIDENCE_REGEXP, line):
                    value = re.search(X_DSPAM_CONFIDENCE_REGEXP, line).group(0).split(' ')[1]
                    block['X_DSPAM_confidence'] = value
                    continue
                # нашли X-DSPAM-Probability
                if re.match(X_DSPAM_PROBABILITY_REGEXP, line):
                    value = re.search(X_DSPAM_PROBABILITY_REGEXP, line).group(0).split(' ')[1]
                    block['X_DSPAM_probability'] = value
                    continue

        return blocks
