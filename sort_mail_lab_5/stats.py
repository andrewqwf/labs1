from collections import defaultdict
from itertools import groupby

DEFAULT_FACTOR = 0.5


class Statistic:
    '''Ститистика по автору'''

    def __init__(self):
        self.msg_count = 0
        self.xdp_count = 0
        self.xdc_count = 0

    def add_msg_info(self, xdp, xdc):
        '''Добавить данные по письму в статистику'''
        self.msg_count += 1
        self.xdp_count += float(xdp)
        self.xdc_count += float(xdc)

    @property
    def is_spammer(self):
        return self.xdp_count / self.msg_count > DEFAULT_FACTOR

    def __iter__(self):
        data = {
            'msg_count': self.msg_count,
            'xdp_count': self.xdp_count,
            'xdc_count': self.xdc_count
        }
        for item in data.items():
            yield item


class Statistics:
    '''Статистика по всем авторам'''

    def __init__(self):
        self.stats = defaultdict(dict)
        self.spammers = []

    def analyze_data(self, data):
        '''Анализ данных'''
        # Группируем входные данные по пользователю
        data = groupby(data, lambda d: d['author'])
        for author, group in data:
            stat = Statistic()
            for value in group:
                stat.add_msg_info(
                    value['X_DSPAM_probability'],
                    value['X_DSPAM_confidence']
                )
            if stat.is_spammer:
                self.spammers.append(author)
            self.stats[author] = dict(stat)

        return self.stats

    def print_spammers(self):
        '''Распечатать список спамеров'''
        if not self.spammers:
            print('Спамеры не обнаружены')
            return

        print('Возможные спамеры:')
        [print(spammer) for spammer in self.spammers]
